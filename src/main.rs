use clap::{App, Arg};
use kochab::{Document, Response, Request, Server};
use kochab::user_management::UserManagementRoutes;
use kochab::user_management::user::RegisteredUser;
use log::*;
use once_cell::sync::OnceCell;
use serde::{Serialize, Deserialize};
use smallvec::SmallVec;

use std::convert::{TryFrom, TryInto};
use std::fmt::Display;

const RANDOM_SEED: u128 = 2018011419180907082019;//u128::from_le_bytes(*b"Trans rights!!\x13\x12");
const VIEW_SIZE: usize = 6;
const MAX_PATH_LEN: usize = 4096; // Setting this small will improve performance at cost of maze quality
static MAZE: OnceCell<Maze> = OnceCell::new();

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    env_logger::init();

    let matches = App::new("Mice in Space")
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about("A simple game about mice in an infinite maze, designed for Gemini")
        .arg(Arg::new("data")
             .about("The directory mice_in_space will store users and maze tiles in")
             .short('d')
             .long("data")
             .default_value("data")
         )
        .arg(Arg::new("certs")
             .about("The directory mice_in_space will store the SSL key and certificate in")
             .short('c')
             .long("certs")
             .default_value("certs")
         )
        .arg(Arg::new("socket")
             .about("The path to the UNIX socket to listen at")
             .default_value("/var/run/mice_in_space.sock")
         )
        .get_matches();

    let db = sled::open(matches.value_of("data").unwrap()).unwrap();

    let maze = Maze::new(&db);
    maze.ensure_established().await;
    MAZE.set(maze).unwrap_or_else(|_|panic!());

    let landing = Response::success_gemini(include_str!("../README.gmi"));

    Server::new()
        .set_database(db)
      //.set_tls_dir(matches.value_of("certs").unwrap())
        .add_um_routes::<UserData>()
        .add_route("/", landing)
        .add_authenticated_route("/app", maze_base)
        .add_authenticated_route("/app/leaderboard", maze_leaderboard)
        .add_authenticated_input_route("/app/etch", "Please enter a message", maze_etch)
        .set_autorewrite(true)
        .serve_unix(matches.value_of("socket").unwrap())
        .await
}

async fn maze_base(req: Request, mut user: RegisteredUser<UserData>) -> anyhow::Result<Response> {
    if let Some(direction) =
        req.query()
            .and_then(|q| q.chars().next())
            .and_then(|c| c.try_into().ok())
    {
        move_user(&mut user, direction).await;
    }
    let mut out = Document::new();
    let UserData { x, y, .. } = user.as_ref();
    let maze = MAZE.get().unwrap();
    let mut view = maze.get_view_around(*x, *y).await;
    let mut same_square_users = SmallVec::<[RegisteredUser<UserData>; 8]>::new();

    for other_user in req.user_manager()
        .all_users::<UserData>()
        .into_iter()
        .filter(|o| o.username() != user.username())
    {
        let UserData { x: ox, y: oy, .. } = other_user.as_ref();

        if (view.left..VIEW_SIZE as i64 +view.left).contains(ox) &&
           (view.bottom..VIEW_SIZE as i64 +view.bottom).contains(oy) {
            view.add_decoration_global(*ox, *oy, "🐁");
        }

        if ox == x && oy == y {
            same_square_users.push(other_user);
        }
    }

    view.add_decoration_global(*x, *y, "🐀");

    out.add_preformatted_with_alt(
        "A maze with a mouse in the middle",
        view.to_string()
    )
       .add_blank_line()
       .add_link("/app?l", "Left")
       .add_link("/app?d", "Down")
       .add_link("/app?u", "Up")
       .add_link("/app?r", "Right")
       .add_blank_line()
       .add_heading(kochab::document::HeadingLevel::H2, "Etchings")
       .add_blank_line()
       .add_link("/app/etch", "Etch something into the wall");
    let tile = maze.tile_at(*x, *y).await;
    if !tile.etchings.is_empty() {
        out.add_blank_line()
           .add_text("Some words are etched into the wall here.");
        for etching in tile.etchings.iter() {
            etching.to_doc(out.add_blank_line())
        }
    }
    if !same_square_users.is_empty() {
        out.add_blank_line()
           .add_heading(kochab::document::HeadingLevel::H2, "Users")
           .add_blank_line()
           .add_text("You spot these fellow rats also milling about here:");
        for other_user in same_square_users.iter() {
            out.add_unordered_list_item(other_user.username());
        }
    }
    out.add_blank_line()
       .add_heading(kochab::document::HeadingLevel::H2, "Misc")
       .add_blank_line()
       .add_link("/account"                  , "Account Settings")
       .add_link("/app/leaderboard"          , "Leaderboard"     )
       .add_link(env!("CARGO_PKG_REPOSITORY"), "Source Code"     );
    Ok(out.into())
}

async fn move_user(user: &mut RegisteredUser<UserData>, direction: Direction) {
    let UserData { x, y, distance_from_start } = user.mut_data();
    let maze = MAZE.get().unwrap();
    let tile = maze.tile_at(*x, *y).await;
    let can_move = match direction {
        Direction::Up    => tile.top,
        Direction::Left  => tile.left,
        Direction::Down  => maze.tile_at(*x, *y - 1).await.top,
        Direction::Right => maze.tile_at(*x + 1, *y).await.left,
    };
    if can_move {
        if (0, 0) == (*x, *y) || direction != tile.to_start {
            *distance_from_start += 1;
        } else {
            *distance_from_start -= 1;
        }
        direction.mv(x, y);
    }
}

async fn maze_etch(
    _: Request,
    user: RegisteredUser<UserData>,
    input: String
) -> anyhow::Result<Response> {
    let UserData { x, y, .. } = user.as_ref();
    info!("{} added an etching at ({}, {}): \"{}\"", user.username(), *x, *y, input);
    MAZE.get().unwrap().tile_at(*x, *y).await.etch(input, user.username());
    Ok(Response::redirect_temporary("/app"))
}

async fn maze_leaderboard(
    req: Request,
    _: RegisteredUser<UserData>,
) -> anyhow::Result<Response> {
    let mut leaderboard = req.user_manager()
        .all_users::<UserData>();
    leaderboard.sort_unstable_by_key(|usr| usr.data().distance_from_start);

    let mut doc = Document::new();
    doc.add_heading(kochab::document::HeadingLevel::H1, "Leaderboard")
       .add_blank_line()
       .add_heading(kochab::document::HeadingLevel::H2, "Furthest from Start")
       .add_blank_line();
    for (pos, user) in leaderboard.iter().enumerate() {
        doc.add_text(format!(
                "{}. {} ({} paces from start)",
                pos + 1, user.username(),
                user.data().distance_from_start,
        ));
    }
    doc.add_blank_line()
       .add_link("/app", "Back");

    Ok(doc.into())
}

#[derive(Debug, Default, Deserialize, Clone, Serialize)]
struct UserData {
    x: i64,
    y: i64,
    distance_from_start: u64,
}

struct Maze {
    tile_tree: sled::Tree,
    etch_tree: sled::Tree,
    generation_lock: tokio::sync::Semaphore,
}

impl Maze {
    fn new(db: &sled::Db) -> Self {
        Self {
            tile_tree: db.open_tree("gay.emii.mice_in_space.tiles").unwrap(),
            etch_tree: db.open_tree("gay.emii.mice_in_space.etchings").unwrap(),
            generation_lock: tokio::sync::Semaphore::new(1),
        }
    }

    #[allow(clippy::missing_const_for_fn)]
    fn key(x: i64, y: i64) -> [u8; 16] {
        let x = x.to_le_bytes();
        let y = y.to_le_bytes();
        let mut out = [0; 16];
        let mut i = 0;
        while i < 8 {
            out[i] = x[i];
            i += 1;
        }
        while i < 16 {
            out[i] = y[i - 8];
            i += 1;
        }
        out
    }

    async fn ensure_established(&self) {
        if !self.tile_tree.contains_key(Self::key(0, 0)).unwrap() {
            info!("Begining initial population of maze.  This may take a while");
            Tile::new(&self.tile_tree, &self.etch_tree, 0, 0, 0, 1);
            self.populate( 0,  1).await;
            self.populate( 1,  1).await;
            self.populate( 1,  0).await;
            self.populate(-1,  0).await;
            self.populate(-1,  1).await;
            self.populate( 0, -1).await;
            self.populate( 1, -1).await;
            self.populate(-1, -1).await;
            info!("Initial maze population finished");
        }
    }

    async fn tile_at(&self, x: i64, y: i64) -> Tile<'_> {
        // In order to guarantee that a tile won't change, we need to ensure that it's top
        // and left neighbors exist
        self.populate(x - 1, y).await;
        self.populate(x, y + 1).await;

        if let Some(tile) = self.populate(x, y).await {
            tile
        } else {
            Tile::load(&self.tile_tree, &self.etch_tree, x, y).unwrap()
        }
    }

    async fn populate(&self, x: i64, y: i64) -> Option<Tile<'_>> {
        if !self.tile_tree.contains_key(Self::key(x, y)).unwrap() {
            Some(self.random_walk(x, y).await)
        } else {
            None
        }
    }

    async fn random_walk(
        &self,
        mut x: i64,
        mut y: i64,
    ) -> Tile<'_> {
        let gen_lock = self.generation_lock.acquire().await;
        let mut state = RANDOM_SEED;
        pcg64_fakehash_pos(&mut state, x, y);
        let mut path = SmallVec::<[(i64, i64); 128]>::new();
        path.push((x, y));

        while !self.tile_tree.contains_key(Self::key(x, y)).unwrap() {
            let random = pcg64(&mut state);
            if random & 1 == 1 {
                x += 1 - (random & 2) as i64
            } else {
                y += 1 - (random & 2) as i64
            };
            /*
            (x, y) = loop {
                let random = pcg64(state);
                let (nx, ny) = if random & 1 == 1 {
                    (x + 1 - (random & 2) as i64, y)
                } else {
                    (x, y + 1 - (random & 2) as i64)
                };
                if let (-4..=4, -4..=4) = (nx, ny) {
                    break (nx, ny);
                }
            };
            */

            if let Some(i) = path.iter().position(|(tx, ty)| *tx == x && *ty == y) {
                trace!("Revisit ({}, {}) @ {}", x, y, path.len());
                for _ in i + 1..path.len() {
                    path.pop();
                }
            } else if path.len() == MAX_PATH_LEN {
                debug!("Max path len reached, resetting to 10%");
                while path.len() > MAX_PATH_LEN / 10 {
                    path.pop();
                }
                let (nx, ny) = *path.last().unwrap();
                x = nx;
                y = ny;
                //(x, y) = *path.last().unwrap();
            } else {
                trace!("Visit ({}, {}) @ {}", x, y, path.len());
                path.push((x, y));
            }
        }

        let (last_x, last_y) = path.last().unwrap();
        debug!("{} tiles generated off ({}, {})", path.len() - 1, last_x, last_y);
        let mut path = path.into_iter().rev();
        let last_tile = path.next().unwrap();
        let mut last_tile = Tile::load(&self.tile_tree, &self.etch_tree, last_tile.0, last_tile.1).unwrap();
        last_tile.save = true;
        for tile in path {
            let mut new_tile = Tile::new(&self.tile_tree, &self.etch_tree, tile.0, tile.1, last_tile.x, last_tile.y);
            new_tile.mutually_open_to(&mut last_tile);
            last_tile = new_tile;
        }
        last_tile.save_immediate();
        drop(gen_lock);
        last_tile
    }

    async fn get_view_around(&self, x: i64, y: i64) -> MazeView<'_> {
        let bottom = y - (VIEW_SIZE as i64 - 1)/2 - 1;
        let left   = x - (VIEW_SIZE as i64 - 1)/2;
        let mut tiles = SmallVec::new();
        let mut decorations = SmallVec::new();
        for i in 0..VIEW_SIZE*VIEW_SIZE {
            let x = left + (i%VIEW_SIZE) as i64;
            let y = bottom + (i/VIEW_SIZE) as i64;
            let tile = self.tile_at(x, y).await;
            if !tile.etchings.is_empty() {
                decorations.push((i, "`,"));
            }
            tiles.push(tile);
        }
        MazeView { tiles, bottom, left, decorations }
    }
}

struct MazeView<'a> {
    pub bottom: i64,
    pub left: i64,
    tiles: SmallVec::<[Tile<'a>; VIEW_SIZE * VIEW_SIZE]>,
    decorations: SmallVec::<[(usize, &'static str); 4]>,
}

impl MazeView<'_> {
    fn tile_at(&self, x: usize, y: usize) -> ViewTile {
        let indx = x/2 + y/2 * VIEW_SIZE;
        match (x % 2, y % 2) {
            (0, 0) => self.tiles[indx].left.into(),
            (0, 1) => ViewTile::Solid,
            (1, 0) => {
                let decor = self.decorations.iter()
                    .rfind(|(i, _)| *i == indx);
                if let Some((_, decor)) = decor {
                    (*decor).into()
                } else {
                    ViewTile::Empty
                }
            },
            (1, 1) => self.tiles[indx].top.into(),
            _ => panic!("Unreachable")
        }
    }

    fn map_global_to_relative(&self, x: i64, y: i64) -> (usize, usize) {
        let (x, y) = (x - self.left, y - self.bottom);
        debug_assert!(x >= 0);
        debug_assert!(y >= 0);
        let (x, y) = (x as usize, y as usize);
        debug_assert!(x < VIEW_SIZE);
        debug_assert!(y < VIEW_SIZE);
        (x, y)
    }

    fn add_decoration(&mut self, x: usize, y: usize, dec: &'static str) {
        self.decorations.push((x + y * VIEW_SIZE, dec));
    }

    fn add_decoration_global(&mut self, x: i64, y: i64, dec: &'static str) {
        let (x, y) = self.map_global_to_relative(x, y);
        self.add_decoration(x, y, dec);
    }
}

impl ToString for MazeView<'_> {
    fn to_string(&self) -> String {
        let mut out = String::with_capacity(2*VIEW_SIZE * (2*VIEW_SIZE - 1));
        for y in (1..VIEW_SIZE*2).rev() {
            for x in 0..VIEW_SIZE*2-1 {
                out.push_str((&self.tile_at(x, y)).into());
            }
            out.push('\n');
        }
        out
    }
}

enum ViewTile {
    Solid,
    Empty,
    Decoration(&'static str),
}

impl From<bool> for ViewTile {
    fn from(is_open: bool) -> Self {
        if is_open { Self::Empty } else { Self::Solid }
    }
}

impl From<&'static str> for ViewTile {
    fn from(decor: &'static str) -> Self {
        Self::Decoration(decor)
    }
}

impl Display for ViewTile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s: &'static str = self.into();
        write!(f, "{}", s)
    }
}

impl From<&ViewTile> for &'static str {
    fn from(tile: &ViewTile) -> Self {
        match tile {
            ViewTile::Solid => "██",
            ViewTile::Empty => "  ",
            ViewTile::Decoration(dec) => *dec,
        }
    }
}

#[derive(Clone, Debug)]
struct Tile<'a> {
    pub top: bool,
    pub left: bool,
    pub x: i64,
    pub y: i64,
    pub save: bool,
    pub etchings: Vec<Etching>,
    pub to_start: Direction,
    tile_tree: &'a sled::Tree,
    etch_tree: &'a sled::Tree,
}

impl<'a> Tile<'a> {
    fn mutually_open_to(&mut self, othr: &mut Tile) {
        let difx = othr.x - self.x;
        let dify = othr.y - self.y;
        match (difx, dify) {
            ( 1,  0) => othr.left = true,
            (-1,  0) => self.left = true,
            ( 0,  1) => self.top = true,
            ( 0, -1) => othr.top = true,
            _ => panic!("Tile::mutually_open_to called on non-adjecent tiles"),
        };
    }

    fn save_immediate(&mut self) {
        let key = Maze::key(self.x, self.y);

        let val = self.to_start.as_u8() |
            if self.top                  { 0b00100 } else { 0 } |
            if self.left                 { 0b01000 } else { 0 } |
            if !self.etchings.is_empty() { 0b10000 } else { 0 };
        self.tile_tree.insert(key, &[val]).unwrap();

        if !self.etchings.is_empty() {
            let val = bincode::serialize(&self.etchings).unwrap();
            self.etch_tree.insert(key, val).unwrap();
        }

        self.save = false;
    }

    fn load(tile_tree: &'a sled::Tree, etch_tree: &'a sled::Tree, x: i64, y: i64) -> Option<Self> {
        let key = Maze::key(x, y);
        tile_tree.get(key).unwrap().map(|bytes| {
            let byte = bytes.as_ref()[0];
            let mut tile = Tile {
                to_start: ( byte & 0b11 ).into(),
                top:  byte & 0b0100 > 0,
                left: byte & 0b1000 > 0,
                x,
                y,
                save: false,
                etchings: Vec::new(),
                tile_tree,
                etch_tree,
            };
            if byte & 0b10000 > 0 {
                let bytes = etch_tree.get(key)
                    .unwrap()
                    .expect("Null reference in database, probably corrupt");
                let bytes = bytes.as_ref();
                tile.etchings = bincode::deserialize(bytes)
                    .expect("Received malformed data from database");
            }
            tile
        })
    }

    fn new(tile_tree: &'a sled::Tree, etch_tree: &'a sled::Tree, x: i64, y: i64, previous_x: i64, previous_y: i64) -> Self {
        let to_start = match (previous_x - x, previous_y - y) {
            ( 1,  0) => Direction::Right,
            (-1,  0) => Direction::Left,
            ( 0,  1) => Direction::Up,
            ( 0, -1) => Direction::Down,
            _ => panic!("Tile created next to non-adjacent tile"),
        };
        Self {
            top: false,
            left: false,
            x,
            y,
            save: true,
            tile_tree,
            etch_tree,
            etchings: Vec::new(),
            to_start,
        }
    }

    fn etch(&mut self, message: impl ToString, author: impl ToString) {
        #[cfg_attr(not(feature = "chrono"), allow(unused_mut))]
        let mut author = author.to_string();
        #[cfg(feature = "chrono")]
        author.push_str(format!(", {}", chrono::Utc::now().date().naive_utc()).as_str());
        self.etchings.push(Etching {
            text: message.to_string(),
            author,
        });
        self.save = true;
    }
}

impl Drop for Tile<'_> {
    fn drop(&mut self) {
        if self.save {
            self.save_immediate()
        }
    }
}

#[derive(Clone, Deserialize, Serialize, Debug)]
struct Etching {
    text: String,
    author: String,
}

impl Etching {
    fn to_doc(&self, doc: &mut Document) {
        doc.add_quote(self.text.as_str())
           .add_quote(format!("   - {}", self.author));
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Direction { Up, Down, Left, Right }
impl Direction {
    const fn as_u8(&self) -> u8 {
        match self {
            Self::Left  => 0,
            Self::Down  => 1,
            Self::Up    => 2,
            Self::Right => 3,
        }
    }
    fn mv(&self, x: &mut i64, y: &mut i64) {
        match self {
            Self::Left  => *x -= 1,
            Self::Down  => *y -= 1,
            Self::Up    => *y += 1,
            Self::Right => *x += 1,
        }
    }
}
#[allow(clippy::fallible_impl_from)]
impl From<u8> for Direction {
    fn from(bits: u8) -> Self {
        match bits {
            0 => Self::Left,
            1 => Self::Down,
            2 => Self::Up,
            3 => Self::Right,
            _ => panic!("Not a valid directional bit"),
        }
    }
}
impl TryFrom<char> for Direction {
    type Error = char;

    fn try_from(c: char) -> Result<Self, char> {
        match c {
            'l' => Ok(Self::Left),
            'd' => Ok(Self::Down),
            'u' => Ok(Self::Up),
            'r' => Ok(Self::Right),
            _ => Err(c),
        }
    }
}

pub fn pcg64(state: &mut u128) -> u64 {
    let mut  x = *state;
    pcg64_iterstate(state);
    let count = (x >> (128 - 6)) as u32; // Highest 6 bits
    x ^= x >> 35;
    ((x >> 58) as u64).rotate_right(count)
}

pub fn pcg64_iterstate(state: &mut u128) {
    const MULTIPLIER: u128 = 0xde92a69f6e2f9f25fd0d90f576075fbd;
    const INCREMENT: u128 = 621;
    *state = state.wrapping_mul(MULTIPLIER).wrapping_add(INCREMENT);
}

pub fn pcg64_fakehash(state: &mut u128, bytes: &[u8]) {
    pcg64_fakehash_inner(state, bytes);
    pcg64_iterstate(state);
}

pub fn pcg64_fakehash_pos(state: &mut u128, x: i64, y: i64) {
    pcg64_fakehash_inner(state, &x.to_le_bytes());
    pcg64_fakehash_inner(state, &y.to_le_bytes());
    pcg64_iterstate(state);
}

fn pcg64_fakehash_inner(state: &mut u128, bytes: &[u8]) {
    let mut i = 0;
    while i < bytes.len() {
        *state = (*state ^ bytes[i] as u128).rotate_right(8);
        i += 1;
    }
}
